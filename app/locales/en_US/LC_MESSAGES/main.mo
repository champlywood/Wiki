��    Q      �  m   ,      �  r   �  !   T     v     �  �   �     3  #   H     l     �     �  +   �     �     �     �  	   �     �     	     	     	     $	     ;	  ,   Y	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	     
     
     $
     3
     F
     X
     h
     u
     �
     �
     �
     �
  	   �
     �
     �
                    0     H     L  :   ]     �     �     �  
   �     �     �     �               ,  	   ?     I     P     V     f  ,   s  
   �  9   �     �     �               +     B  %   Q     w  +   �  �  �  p   �          "     (  }   =     �     �     �     �       (        4     <     J     `     f     n     z     �  	   �     �  ,   �     �     �     �     �                    !     $     1     >     M     [     j     w  
   �     �     �     �     �     �     �     �     �     �                    7     ;  0   I     z     �     �     �     �     �     �     �     �     �     �     �                 #     
   9  0   D     u  	   �     �     �     �     �     �     �     �     @   F   /   	         B                          *   I          "                             J   ;   <   N       Q          H             +   >   ,       E   (   5                     $   &   1          K         M      -   7          C            :   )   .   6                  9           2   ?   P   
   O      %   3                  !   G   8   L       =   0   D       4   '   #   A                  Activer la double identification (nécessite <a href="https://github.com/andOTP/andOTP">AndOTP</a> ou similaire) ? Activer la réécriture d’URL ? Aujourd’hui Autoriser la libre lecture ? Cette action est irréversible. Vous allez définitivement supprimer cette page. Merci de taper le nom de la page « <b>%s</b> » pour confirmer. Changements récents Choix de la langue de l’interface Choix du fuseau horaire Code OTP Commentaire Comparaison entre la version du %s et du %s Comparer Configuration Configuration générale Connexion Contenu Conçu avec %s Courriel Date Dernière modification Durée de l'OTP (en secondes) Décalage (en secondes) avec votre ordiphone Déconnexion Enregistrer le code TOTP Exporter Feuille de style personnalisée Fichiers Format Historique IP Il y a %d ans Il y a %d heures Il y a %d minutes Il y a %d mois Il y a %d secondes Il y a %d semaine Il y a %s jours Il y a un an Il y a un mois Il y a une heure Il y a une minute Il y a une semaine Impossible de créer le fichier Installer Le titre de votre Wiki Modifier Mot de passe Nom Nom de la page d’accueil Nombre de modifications OTP Personnalisation Phrase de passe (laisser vide pour conserver l’actuelle) Plan du site Racine de votre wiki Recherche pour « %s » Rechercher Regénérer un jeton Renommer Restaurer cette version Sauvegarder Se connecter Se souvenir de moi Supprimer Taille Titre Téléversement Téléverser Une nouvelle version est disponible (%s, %s) Version du Veuillez préciser le <i>slug</i> de votre nouvelle page. Votre courriel Votre jeton OTP Votre phrase de passe la locale existe la locale n'existe pas À l’instant Écraser le(s) fichier(s) existant(s) Édition Érreur le fichier n’est pas téléversé Project-Id-Version: 
PO-Revision-Date: 2023-08-05 16:42+0200
Last-Translator: 
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ../..
X-Poedit-SearchPath-0: app/templates/article.php
X-Poedit-SearchPath-1: app/templates/atom.php
X-Poedit-SearchPath-2: app/templates/config.php
X-Poedit-SearchPath-3: app/templates/edit.php
X-Poedit-SearchPath-4: app/templates/history.php
X-Poedit-SearchPath-5: app/templates/history-compare.php
X-Poedit-SearchPath-6: app/templates/history-view.php
X-Poedit-SearchPath-7: app/templates/index.php
X-Poedit-SearchPath-8: app/templates/install.php
X-Poedit-SearchPath-9: app/templates/login.php
X-Poedit-SearchPath-10: app/templates/search.php
X-Poedit-SearchPath-11: app/templates/upload.php
X-Poedit-SearchPath-12: app/inc/app.php
X-Poedit-SearchPath-13: app/inc/functions.php
X-Poedit-SearchPath-14: index.php
 Enable two factors authentification (requires <a href="https://github.com/andOTP/andOTP">AndOTP</a> or similar)? Enable the URL rewriting? Today Enable Open Reading? This action is irreversible, you will permanently delete this page. Please type the name of the page "<b>%s</b> " to confirm. Recent changes Selecting your language Selecting your timezone OTP Code Comment Comparison between the %s and %s version Compare Configuration General configuration Login Content Power by %s E-mail Date Last edit OTP Duration (in seconds) Time difference (in seconds) with your phone Log-Out Save the TOTP code Export Custom style sheet Files Format History IP %d years ago %d hours ago %d minutes ago %d months ago %d seconds ago %d weeks ago %s days ago A year ago One month ago One hour ago One minute ago One week ago Unable to create the file Install Title of your Wiki Edit Password Name Name of home page Number of modifications OTP Customization Passphrase (leave blank to keep the current one) Site Map Root of your Wiki Search for "%s" Search Regenerate a token Rename Restore this version Backup Log-In Remember me Delete Size Title Upload Upload A new version is available (%s, %s) Version of Please specify the <i>slug</i> of your new page. Your e-mail OTP Token Your password the locale exists the local does not exist Just now Overwrite existing file(s) Editing Error file not uploaded 