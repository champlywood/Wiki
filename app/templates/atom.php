<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
	<title><?=$CONFIG['title'];?></title>
	<author><name>Wiki author</name></author>
	<link href="<?=$CONFIG['root'];?>"/>
	<link href="<?=$CONFIG['root'];?>api.php?do=feed" rel="self"/>
	<updated><?=date(DATE_ATOM, time());?></updated>
	<?php foreach($data as $article): ?>
	<entry>
		<title><?=(($article['title'] != '') ? $article['title'] : 'No title'); ?></title>
		<author><name>Wiki Author</name><email><?=$CONFIG['email'];?></email></author>
		<link href="<?=$CONFIG['root'];?>?do=history&amp;view=<?=$article['id'];?>"/>
		<id><?=$CONFIG['root'];?>?do=history&amp;view=<?=$article['id'];?></id>
		<updated><?=date(DATE_ATOM, $article['time']);?></updated>
		<summary><?=$article['comment'];?></summary>
	</entry>
	<?php endforeach; ?>
</feed>
