var editor = new EasyMDE ({
	element: document.getElementById('textarea'),
	promptURLs: true,
	uploadImage: true,
	//imageUploadEndpoint: 'api.php?do=upload',
	imageUploadFunction: UploadPicture,
	imageMaxSize: 1024*1024*20,
	imageUploadAbsoluteURL:false,
	toolbar: [{
                name: "bold",
                action: EasyMDE.toggleBold,
                className: "fa fa-bold",
                title: "Bold",
            },
			{
                name: "italic",
                action: EasyMDE.toggleItalic,
                className: "fa fa-italic",
                title: "Italic",
            },
			{
                name: "strikethrough",
                action: EasyMDE.toggleStrikethrough,
                className: "fa fa-strikethrough",
                title: "Strikethrough",
            },
			{
              name: 'mark',
              action: (e) => {
                e.codemirror.replaceSelection('==' + e.codemirror.getSelection() + '==');
                e.codemirror.focus();
             },
              className: 'fa fa-pencil',
              title: 'Highlight'
            },
			{
              name: 'supertext',
              action: (e) => {
                e.codemirror.replaceSelection('^' + e.codemirror.getSelection() + '^');
                e.codemirror.focus();
             },
              className: 'fa fa-superscript',
              title: 'Super Text'
            },
			{
              name: 'subscript',
              action: (e) => {
                e.codemirror.replaceSelection('~' + e.codemirror.getSelection() + '~');
                e.codemirror.focus();
             },
              className: 'fa fa-subscript',
              title: 'Sub Text'
            },
            "|",
			{
                name: "heading",
                action: EasyMDE.toggleHeadingSmaller,
                className: "fa fa-header",
                title: "Headers",
            },
			{
                name: "heading-smaller",
                action: EasyMDE.toggleHeadingSmaller,
                className: "fa fa-header",
                title: "Smaller Heading",
            },
			{
                name: "heading-bigger",
                action: EasyMDE.toggleHeadingBigger,
                className: "fa fa-lg fa-header",
                title: "Bigger Heading",
            },
			{
                name: "heading-1",
                action: EasyMDE.toggleHeading1,
                className: "fa fa-header header-1",
                title: "Big Heading",
            },
			{
                name: "heading-2",
                action: EasyMDE.toggleHeading2,
                className: "fa fa-header header-2",
                title: "Medium Heading",
            },
			{
                name: "heading-3",
                action: EasyMDE.toggleHeading3,
                className: "fa fa-header header-3",
                title: "Small Heading",
            },
			"|",
			{
                name: "code",
                action: EasyMDE.toggleCodeBlock,
                className: "fa fa-code",
                title: "Code",
            },
			{
                name: "quote",
                action: EasyMDE.toggleBlockquote,
                className: "fa fa-quote-left",
                title: "Quote",
            },
			{
                name: "unordered-list",
                action: EasyMDE.toggleUnorderedList,
                className: "fa fa-list-ul",
                title: "Generic List",
            },
			{
                name: "ordered-list",
                action: EasyMDE.toggleOrderedList,
                className: "fa fa-list-ol",
                title: "Numbered List",
            },
            {
              name: 'checklist',
              action: (e) => {
                e.codemirror.replaceSelection('- [ ]  ');
                e.codemirror.focus();
             },
              className: 'fa fa-check-square',
              title: 'ToDo List'
            },
			"|",
			{
                name: "clean-block",
                action: EasyMDE.cleanBlock,
                className: "fa fa-eraser",
                title: "Clean block",
            },
			"|",
			{
                name: "link",
                action: EasyMDE.drawLink,
                className: "fa fa-link",
                title: "Create Link",
            },
			{
                name: "image",
                action: EasyMDE.drawImage,
                className: "fa fa-picture-o",
                title: "Insert Image",
            },
			{
                name: "heading",
                action: EasyMDE.toggleHeadingSmaller,
                className: "fa fa-header",
                title: "Headers",
            },
			{
                name: "table",
                action: EasyMDE.drawTable,
                className: "fa fa-table",
                title: "Insert Table",
            },
			{
                name: "horizontal-rule",
                action: EasyMDE.drawHorizontalRule,
                className: "fa fa-minus",
                title: "Insert Horizontal Line",
            },
			"|",
			{
                name: "preview",
                action: EasyMDE.togglePreview,
                className: "fa fa-eye no-disable",
                title: "Toggle Preview",
            },
			{
                name: "side-by-side",
                action: EasyMDE.toggleSideBySide,
                className: "fa fa-columns no-disable no-mobile",
                title: "Toggle Side by Side",
            },
			{
                name: "fullscreen",
                action: EasyMDE.toggleFullScreen,
                className: "fa fa-arrows-alt no-disable no-mobile",
                title: "Toggle Fullscreen",
            },
			{
                name: "guide",
                action: MDHelp,
                className: "fa fa-question-circle",
                title: "Markdown Guide",
            },
			/*{
                name: "abc-grammalecte",
                action:  (e) => {
            		oGrammalecteAPI.openPanelForText(easyMDE.codemirror.getValue(), easyMDE.codemirror.display.lineDiv)
             },
                className: "fa fa-check",
                title: "Spell Check (Grammalecte)",
            },
			{
                name: "abc-antidote",
                action: void(0),
                className: "fa fa-check",
                title: "Spell Check (Antidote)",
            },*/
	]
});


function UploadPicture(file, onSuccess, onError) {
    var form_data = new FormData();
    var imageUrl;
    form_data.append('image', file);
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = () => {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status >= 200 && xhr.status < 300) {
          //console.log(xhr.responseText); // 'This is the returned text.'
		  var reponse = xhr.responseText
		  console.log(reponse)
          imageUrl = JSON.parse(reponse)
		  console.log(imageUrl.data.filePath)
          onSuccess(imageUrl.data.filePath)
        } else {
          console.log('Error: ' + xhr.status); // An error occurred during the request.
          onError(xhr.status) // je ne suis pas sûr d'ici
        }
      }
    };
    xhr.open('POST', 'api.php?do=upload');
    xhr.send(form_data);
}
function MDHelp() {
	window.open('https://www.markdownguide.org/basic-syntax/');
}

document.getElementById('textarea').addEventListener("GrammalecteResult", function (event) {
  const detail = (typeof event.detail === 'string') && JSON.parse(event.detail)

  if (detail && detail.sType === 'text') {
    document.getElementById('textarea').value = detail.sText;
  }
})
