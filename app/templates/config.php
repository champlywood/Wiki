<?php if(check($_SESSION['permit']) AND $_SESSION['permit'] == 1): ?>
<div class="block"><a href="api.php?do=backup"><?= t('Sauvegarder');?></a></div>
<form method="post" action="?do=config">
	<fieldset>
		<legend><?= t('Configuration générale');?></legend>
		<label for="title"><?= t('Le titre de votre Wiki');?></label>
		<input type="text" id="title" name="title" value="<?= $CONFIG['title']; ?>"/>
		<label for="root"><?= t('Racine de votre wiki');?></label>
		<input type="url" id="root" name="root" value="<?= $CONFIG['root']; ?>"/>
		<label for="index"><?= t('Nom de la page d’accueil');?></label>
		<input type="text" id="root" name="index" value="<?= $CONFIG['index']; ?>"/>
		<label for="timezone"><?= t('Choix du fuseau horaire');?></label>
		<select name="timezone" id="timezone">
			<?php foreach(beautiful_timezone_list() as $tz=>$tz_name): ?>
			<?php if($CONFIG['timezone'] == $tz): ?>
				<option value="<?= $tz; ?>" selected><?= $tz_name; ?></option>
			<?php else: ?>
				<option value="<?= $tz; ?>"><?= $tz_name; ?></option>
			<?php endif; ?>
			<?php endforeach; ?>
		</select>
		
		<label for="lang"><?= t('Choix de la langue de l’interface');?></label>
		<select name="lang" id="lang">
			<?php foreach($list_lang as $lang): ?>
			<?php if($CONFIG['lang'] == $lang): ?>
				<option value="<?= $lang; ?>" selected><?= $lang; ?></option>
			<?php else: ?>
				<option value="<?= $lang; ?>"><?= $lang; ?></option>
			<?php endif; ?>
			<?php endforeach; ?>
		</select>
		<label for="urlrewrite"><input type="checkbox" id="urlrewrite" name="urlrewrite"  <?=$urlrewrite;?>/> <?= t('Activer la réécriture d’URL ?');?></label>
		<label for="openread"><input type="checkbox" id="openread" name="openread" <?=$openread;?>/> <?= t('Autoriser la libre lecture ?');?></label>
		<label for="openregister"><input type="checkbox" id="openregister" name="openregister" <?=$openregister;?>/> <?= t('Autoriser la libre inscription ?');?></label>
	</fieldset>
	<fieldset>
		<legend><?= t('Personnalisation');?></legend>
		<label for="css"><?= t('Feuille de style personnalisée');?></label>
		<textarea id="css" name="css" rows="25"><?= $css; ?></textarea>
	</fieldset>
	<input type="hidden" name="token" id="token" value="<?= $_SESSION['token'];?>"/>
	<input type="submit"/>
</form>
<table>
	<tr>
		<th><?= t('Courriel');?></th>
		<th><?= t('Pseudo');?></th>
		<th><?= t('Statut');?></th>
		<th><?= t('Actions');?></th>
	</tr>
	<?php foreach($user_data as $d): ?>
	<tr>
		<td><?= $d['email'];?></td>
		<td><?= $d['pseudo'];?></td>
		<td><?= $d['permit'];?></td>
		<td><a href="?do=config&switch=<?=$d['id'];?>&token=<?= $_SESSION['token'];?>"><?= switch_admin($d['permit']);?></a></td>
	</tr>
	<?php endforeach;?>
</table>
<form method="post" action="?do=config">
	<fieldset>
			<legend><?= t('Créer un compte');?></legend>
			<label for="create_account_email"><?= t('Courriel');?></label>
			<input type="email" id="create_account_email" name="create_account_email"/>
			<label for="create_account_pseudo"><?= t('Pseudo');?></label>
			<input type="text" id="create_account_pseudo" name="create_account_pseudo"/>
			<label for="create_account_password"><?= t('Phrase de passe');?></label>
			<input type="password" id="create_account_password" name="create_account_password"/>
			<input type="hidden" name="token" id="token" value="<?= $_SESSION['token'];?>"/>
			<input type="submit"/>
	</fieldset>
<?php endif; ?>
<form method="post" action="?do=config">
	<fieldset>
			<legend><?= t('Connexion');?></legend>
			<label for="pseudo"><?= t('Votre pseudo');?></label>
			<input type="text" id="pseudo" name="pseudo" value="<?= $USER['pseudo']; ?>"/>
			<label for="email"><?= t('Votre courriel');?></label>
			<input type="email" id="email" name="email" value="<?= $USER['email']; ?>"/>
			<label for="pwd"><?= t('Phrase de passe (laisser vide pour conserver l’actuelle)');?></label>
			<input type="password" id="pwd" name="pwd" value="" autocomplete="new-password"/>
			<fieldset>
				<legend><?= t('OTP');?></legend>
				<label for="otp_enable"><input type="checkbox" name="otp_enable" id="otp_enable" <?= $otp_enable;?>/> <?= t('Activer la double identification (nécessite <a href="https://github.com/andOTP/andOTP">AndOTP</a> ou similaire) ?');?></label>
				<label for="otp_gap"><?= t('Décalage (en secondes) avec votre ordiphone');?></label>
				<input type="number" name="otp_gap" id="otp_gap" value="<?=$USER['otp_gap'];?>"/>
				<label for="otp_period"><?= t('Durée de l\'OTP (en secondes)');?></label>
				<input type="number" name="otp_period" id="otp_period" value="<?=$USER['otp_period'];?>"/>
				<label for="otp_token"><?= t('Votre jeton OTP');?> (<a href="?do=config&newtoken=true&token=<?=$_SESSION['token'];?>"><?= t('Regénérer un jeton');?></a>)</label>
				<input type="text" name="otp_token" "pattern"="[A-Z2-7]{8}" value="<?=$USER['otp_token'];?>" disabled="disabled"/>
				<img src="<?=$totpqrcode;?>" alt="qrcode"/>
				<p><a href="<?=$infototp?>"><?= t('Enregistrer le code TOTP');?></a> - Current OTP: <?=$totpnow;?></p>
			</fieldset>
	</fieldset>
	<input type="hidden" name="token" id="token" value="<?= $_SESSION['token'];?>"/>
	<input type="submit"/>
</form>
