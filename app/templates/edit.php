<form method="post" action="?do=edit">
	<fieldset>
		<legend><?= t('Édition');?></legend>
		<label for="title"><?= t('Titre');?></label>
		<input type="text" id="title" name="title" value="<?=$data['title'];?>"/>
		<label for="content"><?= t('Contenu');?></label>
		<textarea id="textarea" name="content"><?=$data['content'];?></textarea>
		<label for="comment"><?= t('Commentaire');?></label>
		<textarea id="comment" name="comment"></textarea>
		<label for="openread"><input type="checkbox" id="openread" name="openread" <?=$openread;?>/> <?= t('Autoriser la libre lecture ?');?></label>
		<input type="hidden" name="slug" id="slug" value="<?=$_GET['id'];?>"/>
		<input type="hidden" name="token" id="token" value="<?=$_SESSION['token'];?>"/>
		<input type="submit"/>
	</fieldset>
</form>
<?php if(check($_SESSION['permit']) AND $_SESSION['permit'] == 1): ?>
<form method="post" action="?do=edit">
	<fieldset>
		<legend><?= t('Renommer');?></legend>
		<label for="new_slug"><?= t('Veuillez préciser le <i>slug</i> de votre nouvelle page.');?></label>
		<input type="text" id="new_slug" name="new_slug"/>
		<input type="hidden" name="slug" id="slug" value="<?=$_GET['id'];?>"/>
		<input type="hidden" name="token" id="token" value="<?=$_SESSION['token'];?>"/>
		<input type="submit"/>
	</fieldset>
</form>
<form method="post" action="?do=edit">
	<fieldset>
		<legend><?= t('Supprimer');?></legend>
		<label for="del_slug"><?= sprintf(t('Cette action est irréversible. Vous allez définitivement supprimer cette page. Merci de taper le nom de la page « <b>%s</b> » pour confirmer.'), $data['slug']); ?></label>
		<input type="text" id="del_slug" name="del_slug"/>
		<input type="hidden" name="slug" id="slug" value="<?=$_GET['id'];?>"/>
		<input type="hidden" name="token" id="token" value="<?=$_SESSION['token'];?>"/>
		<input type="submit"/>
	</fieldset>
</form>
<?php endif; ?>