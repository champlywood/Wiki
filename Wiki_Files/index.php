<?php

error_reporting(-1);
session_start();
include 'app/inc.php';
$config = read_config();
date_default_timezone_set($config['timezone']);
$_GET['id'] = (isset($_GET['id'])) ? $_GET['id'] : null;
authcookie();
if (!file_exists('data') && $_GET['do'] != 'install') {
    redirect('index.php?do=install');
}
echo '<!doctype html>'.PHP_EOL;
echo '<html>'.PHP_EOL;
echo '<head>'.PHP_EOL;
echo "\t".'<meta charset="utf-8"/>'.PHP_EOL;
echo "\t".'<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />'.PHP_EOL;
$title = isset($_GET['id']) ? $config['title'].' - '.read_article($_GET['id'].'/content.md')['title'] : $config['title'].PHP_EOL;
echo "\t".'<title>'.$title.'</title>'.PHP_EOL;
echo "\t".'<link rel="alternate" type="application/atom+xml" href="'.$config['root'].'/api.php?do=feed" title="'.t('content.recent_change').'" />'.PHP_EOL;
echo "\t".'<link href="app/assets/'.$config['stylesheet'].'" type="text/css" rel="stylesheet" />'.PHP_EOL;
echo "\t".'<link href="data/style.css" type="text/css" rel="stylesheet" />'.PHP_EOL;
if (checklogin() || check($config['openwrite'])) {
    echo '<script src="app/lib/sorttable.js"></script>'.PHP_EOL;
    echo "\t".'<script src="app/lib/qr.js"></script>'.PHP_EOL;
    if ($config['easymde'] == true) {
        // https://unpkg.com/easymde/dist/
        echo "\t".'<link rel="stylesheet" href="https://unpkg.com/easymde/dist/easymde.min.css">'.PHP_EOL;
        echo "\t".'<script src="https://unpkg.com/easymde/dist/easymde.min.js"></script>'.PHP_EOL;
    }
}
if ($config['scientific'] == true) {
    echo "\t".'<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.css" integrity="sha384-zB1R0rpPzHqg7Kpt0Aljp8JPLqbXI3bhnPWROx27a9N0Ll6ZP/+DiW/UqRcLbRjq" crossorigin="anonymous">'.PHP_EOL;
    echo "\t".'<script defer src="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.js" integrity="sha384-y23I5Q6l+B6vatafAwxRu/0oK/79VlbSz7Q9aiSZUvyWYIYsd+qj+o24G5ZU2zJz" crossorigin="anonymous"></script>'.PHP_EOL;
    echo "\t".'<script defer src="https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/contrib/auto-render.min.js" integrity="sha384-kWPLUVMOks5AQFrykwIup5lo0m3iMkkHrD0uJ4H5cjeGihAutqP0yW0J6dpFiVkI" crossorigin="anonymous" onload="renderMathInElement(document.body);"></script>'.PHP_EOL;
}
if (file_exists('data/tracker.txt')) {
    file_get_contents('data/tracker.txt');
}
echo '</head>'.PHP_EOL;
echo '<body>'.PHP_EOL;
echo "\t".'<nav>'.PHP_EOL;
echo "\t\t".a($config['root'], $config['title']).PHP_EOL;
if (checklogin() || check($config['openread'])) {
    echo "\t\t".a($config['root'].'/?do=search', t('search')).PHP_EOL;
}
if (checklogin() || check($config['openwrite'])) {
    echo "\t\t".a($config['root'].'/?do=upload', t('media.upload')).PHP_EOL;
    echo "\t\t".a($config['root'].'/?do=index', t('sitemap')).PHP_EOL;
    echo "\t\t".a($config['root'].'/?do=history', t('content.recent_change')).PHP_EOL;
}
if (checklogin()) {
    echo "\t\t".a($config['root'].'/?do=config', t('config')).PHP_EOL;
}
echo "\t\t".a($config['root'].'/?do=login&redirect='.$_GET['id'], ((checklogin()) ? t('auth.logout') : t('auth.login'))).PHP_EOL;
echo "\t".'</nav>'.PHP_EOL;
echo "\t".'<section>'.PHP_EOL;

if (isset($_GET['do'])) {
    switch ($_GET['do']) {
        case 'login':
            if (isset($_SESSION['login'])) {
                $redirect = (isset($_POST['redirect'])) ? url($_POST['redirect']) : $config['root'];
                logout((isset($_GET['redirect'])) ? url($_GET['redirect']) : $config['root']);
            } else {
                $form = new Unicorn\form(array('method'=>'post', 'action'=>$config['root'].'/?do=login'));
                $form->startfieldset(t('auth.login'));
                $form->label('pwd', t('auth.yourpassword'));
                $form->input(array('type'=>'password', 'name'=>'pwd', 'autocomplete'=>'off'));
                $form->input(array('type'=>'hidden', 'name'=>'redirect', 'value'=>(isset($_GET['redirect']) ? $_GET['redirect'] : null)));
                if ($config['otp']['otp_enable'] == true) {
                    $form->label('otp', sprintf(t('config.otp.code'), date('H:i:s', time())));
                    $form->input(array('type'=>'text', 'name'=>'otp', 'autocomplete'=>'off'));
                }
                $form->checkbox('cookie', t('auth.cookie'), array(), false);
                $form->input(array('type'=>'submit', 'value'=>t('send')));
                $form->endfieldset();
                $form->endform();
                if (isset($_POST['pwd'])) {
                    print_r($_POST);
                    $otp = ($config['otp']['otp_enable'] == true) ? $_POST['otp'] : null;
                    $otp = preg_replace('/\s+/', '', $otp);
                    if (check_auth($_POST['pwd'], $otp)) {
                        if (isset($_POST['cookie'])) {
                            setcookie('auth', sha1($config['salt'].$config['pwd']), time() + 30*24*3600, null, null, false, true);
                            mylog('OK COOKIE');
                        }
                        redirect((isset($_POST['redirect'])) ? url($_POST['redirect']) : $config['root']);
                    }
                }
            }
        break;
        case 'install':
            if (!file_exists('data')) {
                $form = new Unicorn\form(array('method'=>'post', 'action'=>'index.php?do=install'));
                $form->startfieldset(t('auth.login'));
                $form->label('pwd', t('config.pwd'));
                $form->input(array('type'=>'password', 'name'=>'pwd', 'autocomplete'=>'new-password'));
                $form->input(array('type'=>'submit', 'value'=>t('send')));
                $form->endform();
                if (isset($_POST['pwd'])) {
                    install($_POST['pwd']);
                    redirect(url('index'));
                }
            }
        break;
        case 'config':
            if (checklogin()) {
                echo check_newversion();
                echo '<div class="block">'.a($config['root'].'/api.php?do=export', t('content.export')).' - '.a($config['root'].'/api.php?do=backup', t('content.backup')).'</div>';
                $form = new Unicorn\form(array('method'=>'post', 'action'=>$config['root'].'/?do=config'));
                $form->startfieldset(t('config.global_config'));
                $form->label('title', t('config.website_title'));
                $form->input(array('type'=>'text', 'name'=>'title', 'value'=>$config['title']));
                $form->label('root', t('config.root'));
                $form->input(array('type'=>'text', 'name'=>'root', 'value'=>$config['root']));
                $form->checkbox('urlrewrite', t('config.urlrewrite'), array(), $config['urlrewrite']);
                $form->label('email', t('config.email'));
                $form->input(array('type'=>'email', 'name'=>'email', 'value'=>$config['email']));
                $form->label('timezone', t('config.timezone'));
                $form->select(array('name'=>'timezone'), beautiful_timezone_list(), $config['timezone'], true);
                $form->label('lang', t('config.lang.default'));
                $lang_list['auto'] = t('config.lang.automatic');
                $form->select(array('name'=>'lang'), $lang_list, $config['lang'], true);
                $form->checkbox('show_public_ip', t('config.show_public_ip'), array(), $config['show_public_ip']);
                $form->endfieldset();

                $form->startfieldset(t('config.customization'));
                $form->label('stylesheet', t('config.stylesheet'));
                $form->select(array('name'=>'stylesheet'), stylesheetlist(), $config['stylesheet'], false);
                $form->label('custom_stylesheet', t('config.custom_stylesheet'));
                $form->textarea(array('name'=>'custom_stylesheet', 'rows'=>'25'), file_get_contents('data/style.css'));
                $form->label('tracker', t('config.tracker'));
                $form->textarea(array('name'=>'tracker', 'rows'=>'25'), file_get_contents('data/tracker.txt'));
                $form->endfieldset();

                $form->startfieldset(t('edition'));
                $form->checkbox('easymde', t('config.easymde'), array(), $config['easymde']);
                $form->checkbox('scientific', t('config.scientific'), array(), $config['scientific']);
                $form->checkbox('history', t('config.history_keep'), array(), $config['history']);
                $form->checkbox('openread', t('config.openread'), array(), $config['openread']);
                $form->checkbox('openwrite', t('config.openwrite'), array(), $config['openwrite']);
                $form->checkbox('notification', t('config.notification'), array(), $config['notification']);
                $form->label('ip_whitelist', t('config.ip_whitelist'));
                $form->input(array('type'=>'text', 'name'=>'ip_whitelist', 'value'=>$config['ip_whitelist']));
                $form->endfieldset();

                $form->startfieldset(t('auth.login'));
                $form->label('pwd', t('config.pwd'));
                $form->input(array('type'=>'password', 'name'=>'pwd', 'autocomplete'=>'new-password'));
                $form->checkbox('otp_enable', t('config.otp.enable'), array(), $config['otp']['otp_enable']);
                $form->label('otp_gap', sprintf(t('config.otp.gap'), date('H:i:s', time())));
                $form->input(array('name'=>'otp_gap', 'type'=>'number', 'value'=>$config['otp']['otp_gap']));
                $form->label('otp_token', t('config.otp.token'));
                $form->input(array('name'=>'otp_token', 'type'=>'text', 'pattern'=> '[A-Z2-7]{8}', 'value'=>$config['otp']['otp_token'], 'disabled'=>'disabled'));
                $totp = OTPHP\TOTP::create($config['otp']['otp_token'], 30, 'sha1', 6);
                $totp->setLabel($config['email']);
                $totp->setIssuer($config['title']);
                echo genqrcode($totp->getProvisioningUri());
                echo 'Current OTP: ' . $totp->now();

                $form->endfieldset();
                $form->input(array('type'=>'hidden', 'name'=>'token', 'value'=>$_SESSION['token']));
                $form->input(array('type'=>'submit', 'value'=>t('send')));
                $form->endform();

                if (isset($_POST['title']) and $_SESSION['token'] == $_POST['token']) {
                    $old_config = $config;
                    $config = array(
                        'title'			=> $_POST['title'],
                        'root'			=> $_POST['root'],
                        'urlrewrite'	=> (isset($_POST['urlrewrite'])) ? true : false,
                        'email'			=> $_POST['email'],
                        'stylesheet'	=> $_POST['stylesheet'],
                        'notification'	=> (isset($_POST['notification'])) ? true : false,
                        'ip_whitelist'	=> $_POST['ip_whitelist'],
                        'history'		=> (isset($_POST['history'])) ? true : false,
                        'openread' 		=> (isset($_POST['openread'])) ? true : false,
                        'openwrite' 	=> (isset($_POST['openwrite'])) ? true : false,
                        'easymde' 		=> (isset($_POST['easymde'])) ? true : false,
                        'scientific' 		=> (isset($_POST['scientific'])) ? true : false,
                        'show_public_ip'=> (isset($_POST['show_public_ip'])) ? true : false,
                        'timezone' 		=> $_POST['timezone'],
                        'lang'			=> $_POST['lang'],
                        'salt'			=> (!isset($config['salt'])) ? sha1(uniqid(rand(), true)) : $config['salt'],
                        'pwd'			=> ($_POST['pwd'] != '') ? password_hash($_POST['pwd'], PASSWORD_DEFAULT) : $config['pwd'],
                        'otp'			=> array(
                                            'otp_enable' 	=> (isset($_POST['otp_enable'])) ? true : false,
                                            'otp_gap'		=> $_POST['otp_gap'],
                                            'otp_token'		=> $_POST['otp_token'])
                    );
                    mylog(t('config.modified').' '.json_encode(array_diff($old_config, $config)));
                    write_config($config);
                    file_put_contents('data/style.css', $_POST['custom_stylesheet']);
                    file_put_contents('data/tracker.txt', $_POST['tracker']);
                    redirect($config['root'].'/?do=config');
                }

                $form = new Unicorn\form(array('method'=>'post', 'action'=>$config['root'].'/?do=config'));
                $form->startfieldset(t('content.purge_history'));
                $form->label('pwd', t('auth.yourpassword'));
                $form->input(array('type'=>'password', 'name'=>'pwd', 'autocomplete'=>'off'));
                $form->input(array('type'=>'hidden', 'name'=>'token', 'value'=>$_SESSION['token']));
                $form->input(array('type'=>'submit', 'name'=>'purge', 'value'=>t('content.purge_history')));
                $form->endfieldset();
                $form->endform();
                if (isset($_POST['purge']) and password_verify($_POST['pwd'], $config['pwd']) and $_POST['token'] == $_SESSION['token']) {
                    purge_history();
                    mylog(t('content.purge_history'));
                }
            } else {
                redirect($config['root']);
            }
        break;

        case 'dev':
            if (checklogin()) {
                echo sprintf(t('config.version'), file_get_contents('version')).'<br>';
                echo 'PHP version is <b>'.phpversion().'</b>';
                config_check('mail');
                config_check('json_decode');
                config_check('json_encode');
                print_r(class_exists('cmark'));

                echo $_SERVER['HTTP_ACCEPT_LANGUAGE'];
                print_r($_COOKIE);
                print_r($_SESSION);
                echo '<hr>';
                echo '<h1>'.t('translation').'</h1>';
                echo '<table class="sortable">';
                echo '<tr>';
                echo '<th></th>';
                foreach ($lang_list as $code_lang => $name_lang) {
                    echo '<th>'.a($config['root'].'/api.php?do=translate&lang='.$code_lang.'&src=fr', $name_lang, 'target="_blank"').'</th>';
                }
                echo '</tr>';
                foreach ($lang['fr'] as $key=>$string) {
                    echo '<tr>';
                    echo '<td data-title="Key">'.$key.'</td>';
                    foreach ($lang_list as $code_lang => $name_lang) {
                        $char = (isset($lang[$code_lang][$key])) ? htmlspecialchars($lang[$code_lang][$key]) : '';
                        echo '<td data-title="'.$name_lang.'">'.$char.'</td>';
                    }
                    echo '</tr>';
                }
                echo '</table>';
                echo '<hr>';
                echo '<div class="log">';
                foreach (array_reverse(file('data/log.log')) as $line) {
                    echo $line;
                }
                echo '</div>';
            }
        break;
        case 'edit':
            if ((checklogin() || check($config['openwrite'])) and (isset($_GET['id']) and file_exists('data/pages/'.$_GET['id'].'/content.md'))) {
                if ((isset($_POST['content']) and isset($_POST['edit'])) || isset($_POST['approve']) and $_GET['id'] !='data/style.css') {
                    $last_version = read_article($_GET['id'].'/content.md');
                    if ($_POST['time_writing'] < $last_version['time']) {
                        echo '<div class="block">'.t('content.time_conflict').'</div>';
                        echo '<div class="diff">'.diffcompare($_POST['content'], $last_version['content']).'</div>';

                        $form = new Unicorn\form(array('method'=>'post', 'action'=>$config['root'].'/?do=edit&id='.$_GET['id']));
                        $form->input(array('type'=>'hidden', 'name'=>'title', 'value'=>$_POST['title']));
                        $form->input(array('type'=>'hidden', 'name'=>'content', 'value'=>$_POST['content']));
                        $form->input(array('type'=>'hidden', 'name'=>'comment', 'value'=>$_POST['comment']));
                        $form->input(array('type'=>'hidden', 'name'=>'time_writing', 'value'=>time()));
                        $form->input(array('type'=>'hidden', 'name'=>'edit', 'value'=>'true'));
                        $form->input(array('type'=>'submit', 'name'=>'approve', 'value'=>t('content.approve')));
                        $form->input(array('type'=>'submit', 'name'=>'reject', 'value'=>t('content.reject')));
                    } elseif (isset($_POST['reject'])) {
                        redirect(url($_GET['id']));
                        mylog(t('content.reject').' '.$_GET['id']);
                    } else {
                        if ($config['history'] == true) {
                            copy('data/pages/'.$_GET['id'].'/content.md', 'data/pages/'.$_GET['id'].'/'.filemtime('data/pages/'.$_GET['id'].'/content.md').'.md');
                        }
                        $article = array(
                                        'title'		=>	$_POST['title'],
                                        'content'	=>	$_POST['content'],
                                        'openread'	=>	((isset($_POST['openread'])) ? true : false),
                                        'time'		=>	time(),
                                        'ip'		=>	$_SERVER['REMOTE_ADDR'],
                                        'comment'	=>	$_POST['comment']
                                    );
                        write_article($_GET['id'], $article);
                        mylog(sprintf(t('content.page_is_modified'), $_GET['id'], $_POST['comment']));
                        redirect(url($_GET['id']));
                    }
                } elseif (isset($_POST['delete']) and checklogin() and ($_POST['token'] == $_SESSION['token'])) {
                    if ($_POST['page_name'] == $_GET['id']) {
                        erased_dir('data/pages/'.$_GET['id']);
                        mylog(sprintf(t('content.delete_page'), $_GET['id']));
                        redirect($config['root']);
                    } else {
                        redirect($config['root'].'/?do=edit&id='.$_GET['id']);
                    }
                } elseif (isset($_POST['rename_page']) and checklogin() and ($_POST['token'] == $_SESSION['token'])) {
                    rename('data/pages/'.$_GET['id'], 'data/pages/'.$_POST['rename_page']);
                    mylog(sprintf(t('content.rename_page'), $_GET['id'], $_POST['rename_page']));
                    redirect($_POST['rename_page']);
                } else {
                    $content = read_article($_GET['id'].'/content.md');
                    $form = new Unicorn\form(array('method'=>'post', 'action'=>$config['root'].'/?do=edit&id='.$_GET['id']));
                    $form->startfieldset(t('edition'));
                    $form->label('title', t('content.title'));
                    $form->input(array('type'=>'text', 'name'=>'title', 'value'=>$content['title'], 'placeholder'=>t('content.title')));
                    $form->label('content', t('content.content'));
                    $form->textarea(array('name'=>'content', 'placeholder'=>t('content.content'), 'rows'=>'25', 'id'=>'textarea'), $content['content']);
                    $form->label('comment', t('content.comment'));
                    $form->textarea(array('name'=>'comment', 'placeholder'=>t('content.comment'), 'rows'=>'5'), '');
                    $form->checkbox('openread', t('config.openread'), array(), $content['openread']);
                    $form->input(array('type'=>'hidden', 'name'=>'time_writing', 'value'=>time()));
                    $form->input(array('type'=>'submit', 'name'=>'edit', 'value'=>t('content.edit')));
                    $form->endfieldset();
                    $form->endform();

                    if (checklogin()) {
                        $form = new Unicorn\form(array('method'=>'post', 'action'=>$config['root'].'/?do=edit&id='.$_GET['id']));
                        $form->startfieldset(t('content.delete'));
                        echo '<p>'.sprintf(t('content.warning_delete'), $_GET['id']).'</p>';
                        $form->input(array('type'=>'text', 'name'=>'page_name'));
                        $form->input(array('type'=>'hidden', 'name'=>'token', 'value'=>$_SESSION['token']));
                        $form->input(array('type'=>'submit', 'name'=>'delete', 'value'=>t('content.delete')));
                        $form->endfieldset();
                        $form->endform();

                        $form = new Unicorn\form(array('method'=>'post', 'action'=>$config['root'].'/?do=edit&id='.$_GET['id']));
                        $form->startfieldset(t('content.rename'));
                        $form->label('rename_page', t('content.rename.label'));
                        $form->input(array('type'=>'text', 'name'=>'rename_page'));
                        $form->input(array('type'=>'hidden', 'name'=>'token', 'value'=>$_SESSION['token']));
                        $form->input(array('type'=>'submit', 'name'=>'rename', 'value'=>t('content.rename')));
                        $form->endfieldset();
                        $form->endform();
                    }
                }
            } else {
                redirect($config['root']);
            }
        break;
        case 'history':
            if ($config['history'] == true and (checklogin() || check($config['openread']))) {
                if (isset($_GET['id']) and file_exists('data/pages/'.$_GET['id'].'/content.md')) {
                    if (isset($_GET['old']) && isset($_GET['new']) and !empty($_GET['old']) && !empty($_GET['new'])) {
                        $old = read_article($_GET['id'].'/'.$_GET['old']);
                        $new = read_article($_GET['id'].'/'.$_GET['new']);
                        if (checklogin() || check($new['openread'])) {
                            echo "\t".'<article>';
                            echo '<div class="block">';
                            if ($_GET['new'] != $_GET['old']) {
                                $old_version = a($config['root'].'/?id='.$_GET['id'].'&view='.$_GET['old'].'&do=history', format_date($old['time']));
                                $new_version = a($config['root'].'/?id='.$_GET['id'].'&view='.$_GET['new'].'&do=history', format_date($new['time']));
                                echo  '<p>'.sprintf(t('content.version_compare'), $old_version, geoip($old['ip']), $new_version, geoip($new['ip'])).'</p>';
                            } else {
                                echo  '<p>'.sprintf(t('content.version_of'), format_date($new['time']), geoip($new['ip'])).'</p>';
                            }
                            $history = array_diff(scandir('data/pages/'.$_GET['id']), array('..', '.'));
                            echo '<p>'.a($config['root'].'/?id='.$_GET['id'].'&do=history', t('content.history')).' - '.a($config['root'].'/?id='.$_GET['id'], t('content.latest_version')).'</p></div>';
                            echo '<div class="diff">'.diffcompare($old['content'], $new['content']).'</div>';
                            echo "\t".'</article>';
                        }
                    } elseif (isset($_GET['view'])) {
                        $content = read_article($_GET['id'].'/'.$_GET['view']);
                        if ((checklogin() || check($config['openread'])) and (checklogin() || check($content['openread']))) {
                            echo "\t".'<article>';
                            echo '<div class="block">';
                            echo '<p>'.sprintf(t('content.old_version'), format_date($content['time']), geoip($content['ip'])).'</p><p>';
                            if ($config['history'] == true) {
                                echo a($config['root'].'/?id='.$_GET['id'].'&do=history', t('content.history')).' - ';
                            }
                            echo a($config['root'].'/'.$_GET['id'], t('content.latest_version')).'</p>';
                            echo '</div>';
                            if ((isset($content['title']) and $content['title'] != '')) {
                                echo "\t\t".'<header>'.PHP_EOL."\t\t\t".'<h1>'.$content['title'].'</h1>'.PHP_EOL."\t\t".'</header>';
                            }
                            echo '<main>'.parse($content['content']).'</main>';
                            echo "\t".'</article>';
                            echo '<p><i>'.$content['comment'].'</i></p>
							<br><textarea id="textarea">'.$content['content'].'</textarea>';
                        }
                    } else {
                        echo  '<p>'.a($config['root'].'/'.$_GET['id'], t('content.view_version'), 'class="button"').'</p>';
                        $history = array_diff(scandir('data/pages/'.$_GET['id'], SCANDIR_SORT_DESCENDING), array('..', '.'));
                        $content = read_article($_GET['id'].'/content.md');
                        $comment = (isset($content['comment']) and $content['comment'] != '') ? ' « <i>'.$content['comment'].'</i> »' : '';
                        echo '<form method="get" action="?do=history&id='.$_GET['id'].'">';
                        echo '<input type="hidden" name="do" value="history">';
                        echo '<input type="hidden" name="id" value="'.$_GET['id'].'">';
                        echo '<input type="submit" value="'.t('content.compare').'">';
                        echo '<table class="sortable">
								<tr>
									<th class="sorttable_nosort">'.t('content.compare').'</th>
									<th>'.t('content.last_edit').'</th>
									<th>'.t('media.size').'</th>
									<th>'.t('content.comment').'</th>
									<th>IP</th>
								</tr>';
                        foreach ($history as $key=>$version) {
                            $content = read_article($_GET['id'].'/'.$version);
                            $empty = (!empty($content['content'])) ? '' : 'class="empty" title="'.t('content.page_not_found').'"';
                            $comment = (isset($content['comment']) and $content['comment'] != '') ? '<i>'.$content['comment'].'</i>' : '-';
                            $compare =  '<input type="radio" name="old" value="'.$history[$key].'"><input type="radio" name="new" value="'.$history[$key].'">';
                            echo '<tr>
									<td data-title="'.t('content.compare').'">'.$compare.'</td>
									<td data-title="'.t('content.last_edit').'" data-st-key="'.$content['time'].'">'.a($config['root'].'/?id='.$_GET['id'].'&view='.$version.'&do=history', format_date($content['time']), $empty).'</td>
									<td data-title="'.t('media.size').'" data-st-key="'.Size('data/pages/'.$_GET['id'].'/'.$version)['bytes'].'">'.Size('data/pages/'.$_GET['id'].'/'.$version)['size'].'</td>
									<td data-title="'.t('content.comment').'">'.$comment.'</td>
									<td data-title="IP">'.geoip($content['ip']).'</td>
								</tr>';
                        }
                        echo '</form>
						</table>';
                    }
                } else {
                    echo '<h1>'.t('content.recent_change').'</h1>';
                    $recentchange = array();
                    foreach (scandir('data/pages') as $page) {
                        if (!in_array($page, $exclude_scan)) {
                            foreach (scandir('data/pages/'.$page) as $file) {
                                if (!in_array($file, array('.','..'))) {
                                    $content = read_article($page.'/'.$file);
                                    $recentchange[$content['time']] = array('content'=>$content['content'], 'comment' => $content['comment'], 'ip' => $content['ip'], 'id'=>$page, 'version'=>$file, 'size'=>Size('data/pages/'.$page.'/'.$file)['size'], 'bytes'=>Size('data/pages/'.$page.'/'.$file)['bytes']);
                                }
                            }
                        }
                    }
                    krsort($recentchange);
                    echo '<table class="sortable">
									<tr>
										<th>'.t('media.name').'</th>
										<th>'.t('content.last_edit').'</th>
										<th>'.t('media.size').'</th>
										<th>'.t('content.comment').'</th>
										<th>IP</th>
									</tr>';
                    foreach ($recentchange as $key => $version) {
                        $version['comment'] = empty($version['comment']) ? '-' : $version['comment'];
                        $empty = (!empty($version['content'])) ? '' : 'class="empty" title="'.t('content.page_not_found').'"';
                        echo '<tr>
							<td data-title="'.t('media.name').'">'.a($version['id'], $version['id'], $empty).'</td>
							<td data-title="'.t('content.last_edit').'" data-st-key="'.$key.'">'.a($config['root'].'/?id='.$version['id'].'&view='.$version['version'].'&do=history', format_date($key)).'</td>
							<td data-title="'.t('media.size').'" data-st-key="'.$version['bytes'].'">'.$version['size'].'</td>
							<td data-title="'.t('content.comment').'">'.$version['comment'].'</td>
							<td data-title="IP">'.geoip($content['ip']).'</td>
						</tr>';
                    }
                    echo '</table>';
                }
            }
        break;
        case 'index':
            if (checklogin() || check($config['openwrite'])) {
                echo '<h1>'.t('content.list_page').'</h1>';
                echo '<table class="sortable">
						<tr>
							<th>'.t('media.name').'</th>
							<th>'.t('content.title').'</th>
							<th>'.t('media.size').'</th>
							<th>'.t('media.size_sum').'</th>
							<th>'.t('content.last_edit').'</th>
							<th>'.t('content.nb_edit').'</th>
						</tr>
				';
                foreach (scandir('data/pages') as $content) {
                    if (!in_array($content, $exclude_scan)) {
                        $empty = (!empty(read_article($content.'/content.md')['content'])) ? '' : 'class="empty" title="'.t('content.page_not_found').'"';
                        echo '<tr>
								<td data-title="'.t('media.name').'">'.a($content, $content, $empty).' ('.a($config['root'].'/?id='.$content.'&do=history', t('content.history')).')</td>
								<td data-title="'.t('content.title').'">'.read_article($content.'/content.md')['title'].'</td>
								<td data-title="'.t('media.size').'" data-st-key="'.Size('data/pages/'.$content.'/content.md')['bytes'].'">'.Size('data/pages/'.$content.'/content.md')['size'].'</td>
								<td data-title="'.t('media.size_sum').'" data-st-key="'.folderSize('data/pages/'.$content)['bytes'].'">'.folderSize('data/pages/'.$content)['size'].'</td>
								<td data-title="'.t('content.last_edit').'" data-st-key="'.read_article($content.'/content.md')['time'].'">'.format_date(read_article($content.'/content.md')['time']).'</td>
								<td data-title="'.t('content.nb_edit').'">'.(count(scandir('data/pages/'.$content))-3).'</td>
							</tr>';
                    }
                }
                echo '</table>';
            }
        break;
        case 'upload':
            if (checklogin() || check($config['openwrite'])) {
                $form = new Unicorn\form(array('method'=>'post', 'enctype'=>'multipart/form-data', 'action'=>$config['root'].'/?do=upload'));
                $form->startfieldset(t('media.upload'));
                $form->input(array('type'=>'file', 'name'=>'file[]', 'size'=>'30', 'multiple'=>'multiple'));
                $form->checkbox('overwrite', t('media.overwrite'), array(), false);
                $form->input(array('type'=>'submit', 'name'=>'upload', 'value'=>t('media.upload')));
                $form->endfieldset();
                $form->endform();
                if (isset($_POST['upload'])) {
                    $_POST['overwrite'] = isset($_POST['overwrite']) ? true : false;
                    $files = array();
                    $fdata = $_FILES['file'];
                    $count = count($fdata['name']);
                    if (is_array($fdata['name'])) {
                        for ($i=0;$i<$count;++$i) {
                            $files[]=array(
                                'name'     => int_to_alph(time()).'_'.$fdata['name'][$i],
                                'tmp_name' => $fdata['tmp_name'][$i],
                                'type' => mime_content_type($fdata['tmp_name'][$i]),
                            );
                        }
                    } else {
                        $files[]=$fdata;
                    }
                    foreach ($files as $file) {
                        if (!is_uploaded_file($file['tmp_name'])) {
                            exit('<div class="block">'.t('media.error.not_upload').'</div>');
                        }
                        $file['name'] = ($_POST['overwrite'] == true) ? $file['name'] : time().$file['name'];
                        if (in_array($file['type'], $blacklist_mimetype)) {
                            exit('<div class="block">'.sprintf(t('media.error.bad_typemime'), $file['type']).'</div>');
                        }
                        if (!move_uploaded_file($file['tmp_name'], 'data/files/'.$file['name'])) {
                            exit('<div class="block">'.t('media.error.cantcreatefile').'</div>');
                        }
                        mylog(sprintf(t('media.upload_file'), $file['name']));
                        echo '<input onclick="this.select()" value="'.mdmedia('data/files/'.$file['name']).'"/>';
                    }
                }

                echo '<h1>'.t('media.files').'</h1>';
                echo '<table class="sortable">
						<tr>
							<th></th>
							<th>'.t('media.name').'</th>
							<th>'.t('media.date').'</th>
							<th>'.t('media.mimetype').'</th>
							<th>'.t('media.size').'</th>
							<th>'.t('content.delete').'</th>
						</tr>
				';
                foreach (scandir('data/files') as $content) {
                    if (!in_array($content, $exclude_scan)) {
                        echo '<tr>
						<td>
							'.affmedia('data/files/'.$content).'
							<input onclick="this.select()" value="'.mdmedia('data/files/'.$content).'"/>
							<samp title="sha-1">'.sha1_file('data/files/'.$content).'</samp>
						</td>
						<td data-title="'.t('media.name').'">'.a('data/files/'.$content, $content, 'download').'</td>
						<td data-title="'.t('media.date').'">'.format_date(filemtime('data/files/'.$content)).'</td>
						<td data-title="'.t('media.mimetype').'">'.mime_content_type('data/files/'.$content).'</td>
						<td data-title="'.t('media.size').'">'.Size('data/files/'.$content)['size'].'</td>
						<td data-title="'.t('content.delete').'">'.a('?do=upload&del='.$content.'&token='.$_SESSION['token'], t('content.delete')).'</td>
						</tr>
						';
                    }
                }
                echo '</table>';
                if (isset($_GET['del']) and checklogin() and $_GET['token'] == $_SESSION['token']) {
                    unlink('data/files/'.$_GET['del']);
                    mylog(sprintf(t('content.delete_file'), $_GET['del']));
                    redirect('?do=upload');
                }
            }
        break;
        case 'search':
            if (checklogin() || check($config['openread'])) {
                $_GET['q'] = isset($_GET['q']) ? $_GET['q'] : '';
                $form = new Unicorn\form(array('method'=>'get', 'action'=>$config['root'].'/?do=search'));
                $form->startfieldset(t('search'));
                $form->input(array('type'=>'text', 'name'=>'q', 'value'=>$_GET['q']));
                $form->input(array('type'=>'hidden', 'name'=>'do', 'value'=>'search'));
                $form->input(array('type'=>'submit', 'value'=>t('send')));
                $form->endfieldset();
                $form->endform();
                if ($_GET['q'] != '') {
                    $searchlist = '<ul>';
                    foreach (scandir('data/pages') as $content) {
                        if (!in_array($content, $exclude_scan)) {
                            $article = read_article($content.'/content.md');
                            $pattern = preg_quote(strtolower($_GET['q']), '/');
                            $pattern = "/^.*$pattern.*\$/m";
                            $contents = strtolower($article['title'].' '.$article['content']);
                            if (preg_match_all($pattern, $contents, $matches)) {
                                $title = ($article['title'] != '') ? $article['title'] : $content;
                                if (checklogin() || check($article['openread'])) {
                                    $searchlist .= '<li>'.a($content, $title).'</li>';
                                }
                            }
                        }
                    }
                    $searchlist .= '</ul>';
                    echo $searchlist;
                }
            }
        break;
    }
} elseif (isset($_GET['id'])) {
    if (file_exists('data/pages/'.$_GET['id'].'/content.md')) {
        $content = read_article($_GET['id'].'/content.md');
        if ((checklogin()|| check($config['openread'])) and (checklogin() || check($content['openread']))) {
            echo "\t".'<article>'.PHP_EOL;
            echo "\t\t".'<div class="block">';
            echo '<p>'.sprintf(t('content.last_edit_date'), format_date($content['time'])).'</p>'.PHP_EOL;
            echo '<p>';
            if ($config['history'] == true) {
                echo ' '.a($config['root'].'/?id='.$_GET['id'].'&do=history', t('content.history')).' ';
            }
            if (@$_SESSION['login'] == true || check($config['openwrite'])) {
                echo ' - '.a($config['root'].'/?id='.$_GET['id'].'&do=edit', t('content.edit')).' ';
            }
            if (checklogin()) {
                echo ' - '.a($config['root'].'/api.php?do=export&id='.$_GET['id'], t('content.export')).' ';
            }
            echo '</p></div>'.PHP_EOL;

            if (isset($content['title']) and $content['title'] != '') {
                echo  "\t\t".'<header>'.PHP_EOL."\t\t\t".'<h1>'.$content['title'].'</h1>'.PHP_EOL."\t\t".'</header>';
            }
            echo '<main>'.parse($content['content']).'</main>';
            echo "\t".'</article>';
        }
    } else {
        if (checklogin() || check($config['openwrite'])) {
            $article = array(
                            'title'=> '',
                            'content'=>'',
                            'openread'=> $config['openread'],
                            'time'=>time(),
                            'ip'=>$_SERVER['REMOTE_ADDR'],
                            'comment'=>'Creation'
                        );
            write_article($_GET['id'], $article);
            mylog(sprintf(t('content.create_page'), $_GET['id']));
            redirect(url($_GET['id']));
        }
    }
} else {
    redirect(url('index'));
}
echo "\t".'</section>'.PHP_EOL;
echo "\t".'<footer>'.PHP_EOL;
if (checklogin()) {
    echo "\t\t".a($config['root'].'/?do=dev', t('dev.section')).PHP_EOL;
}
echo "\t\t".'<p>'.sprintf(t('powered_by'), a('https://framagit.org/qwertygc/Wiki', '🦄 Unicorn Wiki').'').'</p>'.PHP_EOL;
echo "\t".'<footer>'.PHP_EOL;

if ((checklogin() || check($config['openwrite'])) and $config['easymde'] == true) {
    echo "\t".'<script src="app/easymde.js"></script>'.PHP_EOL;
}
echo '</body>'.PHP_EOL;
echo '</html>'.PHP_EOL;
