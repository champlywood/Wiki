# Unicorn Wiki
**Logiciel en bêta, présence probable de bugs**

Unicorn Wiki est un logiciel de wiki personnel écrit en PHP. Ce logiciel est un logiciel amateur, codé sur le temps libre par un non-professionnel. Je suis « dictateur bienveillant » de ce logiciel : je peux décider d'implanter ou non une fonctionnalité, si je la juge nécessaire : ce logiciel a pour vocation première de me rendre service, et je mets à disposition de la communauté. Le maitre mot est simplicité. S’il ne vous convient pas, vous êtes libre de choisir un autre logiciel :-)

Il est traduit en français (langue d'origine), anglais (traduction approximative) et en occitan (merci Quentin).


# Crédits

The Wiki is powered by [Unicorn Wiki](https://framagit.org/qwertygc/Wiki).

Thinks to :

- [CommonMark](https://commonmark.thephpleague.com/)
- [PHPDIFF](https://github.com/jfcherng/php-diff)
- [otPHP](https://github.com/Spomky-Labs/otphp)
- [PHP-Gettext](https://github.com/php-gettext)

- Mercés al Quenti per la traduccion en occitan (et pour le thème Oucitanio)

Thinks to (Files version)

- [Sorttable](https://github.com/White-Tiger/sorttable.js)

- [Easy Markdown Editor](https://github.com/Ionaru/easy-markdown-editor)
- [Parsedown and ParsedownExtra](http://parsedown.org/)
- [Parsedown Extension TOC](https://github.com/KEINOS/parsedown-extension_table-of-contents)
<!--- [Parsedown Extended](https://github.com/BenjaminHoegh/ParsedownExtended)-->

- [PHP-FineDiff](https://github.com/bariew/FineDiff)

- [Unicorn System](https://framagit.org/qwertygc/unicornsystem)

- [otPHP](https://github.com/Spomky-Labs/otphp)
- [qr.js](https://bitbucket.org/lifthrasiir/qrjs/src/default/)

- Mercés al Quenti per la traduccion en occitan (et pour le thème Oucitanio)

# Documentation
## Pour installer le logiciel

1. Installer les dépendances : `composer install`
2. Sur votre serveur, supprimer tous les fichiers sauf `data/`
3. Téléverser les nouveaux fichiers

## Convertir le Wiki version fichier en Wiki version SQL

Pour faire la migration : 1) faire une sauvegarde du dossier data et le renommer en data_files. 2) Installer le wiki dans une instance « neuve ». 3) aller dans api.php?do=files2sql et faire la migration.

## Traduire

Avec la version avec SQL, on est passé avec Gettext. Malheureusement, j'ai dû user d'un petit « hack » pour permettre d'utiliser des langues « exotiques », c'est-à-dire pas installées dans les « locales systèmes » des serveurs. Après avoir traduit les .po (avec Poedit par exemple) il faut aller sur api.php?do=translate pour convertir les fichiers en .json. 

J'ai crée un « fichier source », `app/locales/main.pot` que vous pouvez utiliser pour vos traductions. Enregistrez votre fichier sous le nom `app/locales/[CODE ISO]/LC_MESSAGES/main.po` et `app/locales/[CODE ISO]/LC_MESSAGES/main.mo` où `[CODE ISO]` est le code de votre langage.
